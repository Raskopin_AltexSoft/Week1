﻿using System;
using System.Collections.Generic;


namespace Task4._1
{
    class Student: IComparable<Student>
    {

        private readonly DateTime from = DateTime.Now.AddYears(-120);
        private readonly DateTime to = DateTime.Now.AddYears(-15);
        private DateTime birthday;
        private DateTime graduationDate;
        private Addres addres;

        private struct Addres
        {
            public string Country { get; set; }
            public string Town { get; set; }
            public string Street { get; set; }
            public int BuildingNumber { get; set; }
            public short ApartmentNumber { get; set; }
        }

        public Student(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public void SetAddres(string country, string town, string street, int buildingNumber, short apartmentNumber = 0)
        {
            addres.Country = country;
            addres.Town = town;
            addres.Street = street;
            addres.BuildingNumber = buildingNumber;
            addres.ApartmentNumber = apartmentNumber;
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime GraduationDate
        {
            get { return graduationDate; }
            set {
                if (value > Birthday)
                {
                    graduationDate = value;
                }
            }
        }

        public byte SchoolNumber { get; set; }

        public DateTime Birthday
        {
            get { return birthday; }
            set
            {
                if (value >= from && value <= to)
                birthday = value;
            }
        }
       
        public string GetFullName()
        {
            return FirstName + " " + LastName;
        }

        public void GetAddress()
        {
            Console.Write("Addres: ");
            Console.WriteLine("bld. {0},  {1} street, {2}, {3}", addres.BuildingNumber, addres.Street, addres.Town, addres.Country);
        }

        public int CompareTo(Student student)
        {
            return Birthday.CompareTo(student.Birthday);
        }

        public static List<Student> GetStudentsBySchoolNumber(List<Student> studentsUnordered, int schoolNumber)
        {
            var students = studentsUnordered;
            return students.FindAll(s => s.SchoolNumber == schoolNumber);
        }
    }
}
