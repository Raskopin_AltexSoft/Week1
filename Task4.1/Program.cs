﻿using System;
using System.Collections.Generic;


namespace Task4._1
{
    class Program
    {
        static void Main()
        {
            var numberOfStudents = InputNumberOfStudents();
            var students = FillListOfStudents(numberOfStudents);
            OutputStudents(students);

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine();
            Console.WriteLine("*students in school 1 sorted by birthday*");
            students = Student.GetStudentsBySchoolNumber(students, 1);
            students.Sort();
            OutputStudents(students);

            Console.ReadKey();
        }

        public static byte InputNumberOfStudents()
        {
            byte numberOfStudents;
            Console.WriteLine("Input number of students:");
            while (!byte.TryParse(Console.ReadLine(), out numberOfStudents))
            {
                Console.WriteLine("Value is wrong. Please, try again:");
            }
            return numberOfStudents;
        }

        public static List<Student> FillListOfStudents(byte studentsNumber)
        {
            var students = new List<Student>();
            for (var i = 0; i<studentsNumber; i++)
            {
                var student = new Student(Faker.Name.First(), Faker.Name.Last());
                student.SetAddres(Faker.Address.Country(),
                                  Faker.Address.City(),
                                  Faker.Address.StreetName(),
                                  (short)Faker.RandomNumber.Next(1, 200),
                                  (short)Faker.RandomNumber.Next(1, 200));
                student.Birthday = RandomDay();
                student.SchoolNumber = (byte)Faker.RandomNumber.Next(1,10);
                student.GraduationDate = student.Birthday.AddYears(15+Faker.RandomNumber.Next(0,3));
                students.Add(student);
            }
            return students;
        }
        
        public static DateTime RandomDay()
        {
            var gen = new Random((int)DateTime.Now.Ticks);
            var start = new DateTime(1900, 1, 1);
            var range = (DateTime.Today.AddYears(-18) - start).Days;
            return start.AddDays(gen.Next(range));
        }

        public static void OutputStudents(List<Student> students)
        {
            foreach (var student in students)
            {
                Console.WriteLine("{0}. Birthday: {1}", student.GetFullName(), $"{student.Birthday:d/M/yyyy}");
                student.GetAddress();
                Console.WriteLine("School number: {0}, Graduation day: {1}", student.SchoolNumber, $"{student.GraduationDate:d/M/yyyy}");
                Console.WriteLine(new string('-', 50));
            }
        }
    }
}
