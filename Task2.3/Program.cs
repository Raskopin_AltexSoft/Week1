﻿/*
 * Fill given array random numbers and sort in.   
 */
using System;


namespace Task2._3
{
    class Program
    {
        static void Main()
        {
            byte numberOfElementsInArray = InputNumberOfElementsInArray();
            byte[] array = CreateArray(numberOfElementsInArray);
            OutputArray(array);

            Array.Sort(array);
            Console.WriteLine("Array sorted with standart method:");
            OutputArray(array);

            Console.ReadKey();
        }

        //function creates array with given size
        public static byte[] CreateArray(byte arraySize)
        {
            var array = new byte[arraySize];
            var randomNumber = new Random();
            for (var i = 0; i< arraySize; i++)
            {
                array[i] = (byte)randomNumber.Next(-500, 500);
            }
            return array;
        }

        //user's input
        public static byte InputNumberOfElementsInArray()
        {
            byte numberOfElementsInArray;
            Console.WriteLine("Input number of elements in array:");
            while (!byte.TryParse(Console.ReadLine(), out numberOfElementsInArray))
            {
                Console.WriteLine("Value is wrong. Please, try again:");
            }
            return numberOfElementsInArray;
        }

        //output given array
        public static void OutputArray(byte[] array)
        {
            foreach (var t in array)
            {
                Console.Write("{0} ", t);
            }
            Console.WriteLine();
        }
    }

}
