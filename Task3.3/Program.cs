﻿/*
 * for given directory output all files and subdirectories in. Use recursion for this task.  
 */
using System;
using System.IO;

namespace Task3._3
{
    class Program
    {
        static void Main()
        {
            //root parent of solution
            var path = Directory.GetCurrentDirectory() + @"\..\..\";
            if (Directory.Exists(path))
            {
                var dir = new DirectoryInfo(path);
                Console.WriteLine(dir.Name);
                GetDirectories(dir.FullName, 0);
            }
            Console.ReadKey();
        }

        // recursion function outputs subdirectories names
        static void GetDirectories(string path, int position)
        {
            string[] directories = Directory.GetDirectories(path);
            foreach (var t in directories)
            {
                position += 2;
                Console.Write(new string('-', position));
                Console.WriteLine(new DirectoryInfo(t).Name);
                GetFiles(t, position);
                GetDirectories(t, position);
                position-=2;
            }
        }

        //function outputs files names 
        static void GetFiles(string path, int position)
        {
            position += 2;
            Console.ForegroundColor = ConsoleColor.Red;
            string[] files = Directory.GetFiles(path);
            foreach (var t in files)
            {
                Console.Write(new string('-', position));
                Console.WriteLine(new FileInfo(t).Name);
            }
            Console.ResetColor();
        }
    }

}
