﻿using System;


namespace Task4._3
{
    internal class Passenger: IComparable<Passenger>
    {
        private string firstName;
        private string lastName;
        private readonly int thingsAccount;
        private readonly short weight;

        public Passenger(string firstName, string lastName,int thingsAccount, short weight)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.thingsAccount = thingsAccount;
            this.weight = weight;
        }

        public int ThingsAccount { get { return thingsAccount; }  }
        public short Weight { get { return weight; } }

        public string FullName { get { return firstName + " " + lastName; } }
        public int CompareTo(Passenger other)
        {
            return ThingsAccount.CompareTo(other.ThingsAccount);
        }
    }
}
