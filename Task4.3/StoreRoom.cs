﻿using System.Collections.Generic;


namespace Task4._3
{
    internal class StoreRoom
    {
        private const short MaxWeight = 25;

        private readonly List<Passenger> passengers;

        public StoreRoom(List<Passenger> passsengers)
        {
            passengers = passsengers;
        }

        public List<Passenger> GetPassengers()
        {
            var wrongPassengers = passengers.FindAll(p => p.Weight > MaxWeight);
            wrongPassengers.Sort();
            return wrongPassengers;
        }
    }
}
