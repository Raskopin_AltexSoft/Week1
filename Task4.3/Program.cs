﻿using System;
using System.Collections.Generic;


namespace Task4._3
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var passengers = new List<Passenger>
            {
                new Passenger("Igor", "Petrov", 15, 30),
                new Passenger("Olga", "Ivanova", 7, 15),
                new Passenger("Ivan", "Sidorov", 12, 26)
            };
            var storeRoom = new StoreRoom(passengers);
            foreach (var p in storeRoom.GetPassengers())
            {
                Console.WriteLine("Passenger: {0}, Average weight: {1}, Things acount: {2}", p.FullName, p.Weight, p.ThingsAccount );
            }
            Console.ReadKey();
        }
    }
}
