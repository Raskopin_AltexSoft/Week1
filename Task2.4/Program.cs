﻿/*
 * Fill given MxN array random numbers and sort it by summ of elements in column.   
 */
using System;


namespace Task2._4
{
    class Program
    {
        static void Main()
        {
            byte n, m;  
            InputNumberOfElementsInArray(out n, out m);
            int[,] array = CreateArray(n, m);
            OutputArray(array);

            ColumnsSum(array);
            ColumnsSort(array);
            Console.WriteLine("\nArray after sort:");
            OutputArray(array);
            Console.ReadKey();
        }

        //this function create array with demension NxM and fill it with random numbers
        public static int[,] CreateArray(int n, int m)
        {
            int[,] array = new int[n, m];
            Random randomNumber = new Random();
            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < m; j++)
                {
                    array[i,j] = randomNumber.Next(10, 99);
                }
                
            }
            return array;
        }

        //this function realize user's input of array demension
        public static void InputNumberOfElementsInArray(out byte n, out byte m)
        {
            Console.WriteLine("Input number of rows:");
            while (!byte.TryParse(Console.ReadLine(), out n))
            {
                Console.WriteLine("Value is wrong. Please, try again:");
            }
            Console.WriteLine("Input number of columns:");
            while (!byte.TryParse(Console.ReadLine(), out m))
            {
                Console.WriteLine("Value is wrong. Please, try again:");
            }
        }

        //this function output given array in console
        public static void OutputArray(int[,] array)
        {
            Console.WriteLine();
            for (var i = 0; i < array.GetLength(0); i++)
            {
                for (var j = 0; j< array.GetLength(1); j++)
                {
                    Console.Write("{0} ", array[i,j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        //this function calculate summ for each column in given array
        public static void ColumnsSum(int[,] array)
        {
            for (var i = 0; i < array.GetLength(1); i++)
            {
                var sum = 0;
                for (var j = 0; j < array.GetLength(0); j++)
                {
                    sum += array[j, i];
                }
                Console.WriteLine("Summ of {0} column: {1}", i, sum);
            }
        }

        //this function sort given array by summ of elements in columns (bubble sort)
        public static void ColumnsSort(int[,] array)
        {
            bool swapped;
            for (var i = 0; i < array.GetLength(1)-1; i++)
            {
                var k = 0;
                swapped = false;
                while (k < array.GetLength(1) - 1 - i)
                {
                    var sum0 = 0;
                    var sum1 = 0;
                    for (int j = 0; j < array.GetLength(0); j++)
                    {
                        sum0 += array[j, k];
                        sum1 += array[j, k + 1];
                    }
                    if (sum0>sum1)
                    {
                        for (var j = 0; j < array.GetLength(0); j++)
                        {
                            var temp = array[j, k];
                            array[j, k] = array[j, k + 1];
                            array[j, k + 1] = temp;
                        }
                        swapped = true;
                    }
                    ++k;
                }
                if (!swapped)
                {
                    break;
                }
            }
        }
    }
}
