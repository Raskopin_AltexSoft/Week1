﻿/*
* task 2.2 for existing struct 'Student' create array, then sort it by field FirstName. 
* After sort it by field 'Age'using overload method Sort (IComprarer).
*/
using System;
using System.Collections;


namespace Task2._2
{
    class Program
    {
        static void Main()
        {
            byte numberOfStudents = InputNumberOfStudents();
            Student[] studentsArray = CreateStudensArray(numberOfStudents);

            Console.WriteLine("Output unsortable array of students");
            OutputStudentsArray(studentsArray);

            //Array.Sort(studentsArray, (x, y) => string.Compare(x.FirstName, y.FirstName, StringComparison.Ordinal));
            //or?
            //Array.Sort(studentsArray);  // this is possible because class "Student" extend interface IComparable<>
            //or?
            Student.SortByFirstName(studentsArray);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nOutput array of students sorted by firstnames");
            OutputStudentsArray(studentsArray);
            
            //sort array by  age
            //Array.Sort(studentsArray, (IComparer) new Student());
            //or? 
            Student.SortByAge(studentsArray);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nOutput array of students sorted by age");
            OutputStudentsArray(studentsArray);
            
            Console.ReadKey();
        }

        //function realize user`s input 
        public static byte InputNumberOfStudents()
        {
            byte numberOfStudents;
            Console.WriteLine("Input number of students:");
            while (!byte.TryParse(Console.ReadLine(), out numberOfStudents))
            {
                Console.WriteLine("Value is wrong. Please, try again:");
            }
            return numberOfStudents;
        }

        //function create array of students
        public static Student[] CreateStudensArray(byte numberOfStudents)
        {
            string[] formOfEducation = { "full-time", "part-time" };
            string[] faculty = {
                "Faculty of Architecture and History of Art",
                "Faculty of Music",
                "Faculty of Philosophy",
                "Faculty of Law",
                "Faculty of Biology"
            };
            var studentsArray = new Student[numberOfStudents];
            for (var i = 0; i < numberOfStudents; i++)
            { 
                studentsArray[i].FirstName = Faker.Name.First();
                studentsArray[i].LastName = Faker.Name.Last();
                studentsArray[i].FormOfEducation = formOfEducation[Faker.RandomNumber.Next(0, formOfEducation.Length)];
                studentsArray[i].Course = (sbyte)Faker.RandomNumber.Next(1, 5);
                studentsArray[i].Faculty = faculty[Faker.RandomNumber.Next(0, faculty.Length)];
                studentsArray[i].Age = (sbyte)Faker.RandomNumber.Next(16, 90);
            }
            return studentsArray;
        }

        //function output array of students
        public static void OutputStudentsArray(Student[] studentsArray)
        {
            for (var i = 0; i < studentsArray.Length; i++)
            {
                Console.WriteLine(new string('-', 50));
                Console.WriteLine("{0}: {1} {2}  Age: {3}", i+1, studentsArray[i].FirstName, studentsArray[i].LastName, studentsArray[i].Age);
                Console.WriteLine("faculty: {0}; form of education: {1}; course: {2};", studentsArray[i].Faculty, studentsArray[i].FormOfEducation, studentsArray[i].Course);
            }

        }

    }

}
