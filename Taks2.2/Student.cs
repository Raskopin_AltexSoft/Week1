﻿using System.Collections;
using System;

namespace Task2._2
{
    public struct Student : IComparer, IComparable<Student>
    {

        //method implement IComparable<Student> interaface
        public int CompareTo(Student student)
        {
            return string.Compare(FirstName, student.FirstName, StringComparison.Ordinal);
        }

        //method implement IComparer interaface
        public int Compare(object a, object b)
        {
            if (!(a is Student) || !(b is Student)) return 0;
            var first = (Student)a;
            var second = (Student)b;
            if (first.Age > second.Age)
            {
                return 1;
            }
            if (first.Age < second.Age)
            {
                return -1;
            }
            return 0;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FormOfEducation { get; set; }
        public string Faculty { get; set; }
        public sbyte Course { get; set; }
        public sbyte Age { get; set;  }

        public static void SortByFirstName(Student[] studentsArray)
        {
            Array.Sort(studentsArray);
        }

        public static void SortByAge(Student[] studentsArray)
        {
            Array.Sort(studentsArray, (IComparer)new Student());
        }

    }

}