﻿using System;
using System.Collections.Generic;


namespace Task4._2
{
    class Student : IComparable<Student>
    {
        byte[] examensMarks = new byte[3];

        public int CompareTo(Student student)
        {
            return GroupNumber.CompareTo(student.GroupNumber);
        }

        public Student(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public byte[] ExamenMark
        {
            get { return examensMarks; }
            set
            {
                if (value.Length<=3)
                {
                    examensMarks = value;
                }
            }
        }

        public byte GroupNumber { get; set; }

        public void SetExamenMark(byte mark, byte examenNumber)
        {
          examensMarks[examenNumber] = mark;
        }
        
        public bool IfSessionSuccessful()
        {
            var result = true;
            foreach (var t in examensMarks)
            {
                if (t < 7)
                {
                    result = false;
                }
            }
            return result;
        }

        public string GetFullName()
        {
            return FirstName + " " + LastName;
        }

        public static List<Student> GetSuccessfulyStudents(List<Student> allStudents)
        {
            var successfulyStudents = allStudents;
            return successfulyStudents.FindAll(s => s.IfSessionSuccessful());
        }
    }

}
