﻿//replace numbers in file with its square 
using System;
using System.Text;
using System.IO;
using System.Collections;

namespace Task3._2
{
    class Program
    {
        //number of symbols in file name
        const int acountOfSymbolsInFileName = 8;
        static void Main()
        {
            var acountOfNumbers = InputAcountOfNumbers();
            var path = Directory.GetCurrentDirectory() + @"\..\..\folder\";

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            FileInfo file = CreateOrOpenFile(path);
            FileFill(file, acountOfNumbers);
            int[] numbersArray = FileRead(file);

            Console.WriteLine("Original array form file:");
            OutputArray(numbersArray);

            FileChange(file, numbersArray);
            numbersArray = FileRead(file);

            Console.WriteLine("Changed array form file:");
            OutputArray(numbersArray);

            Console.ReadKey();
        }

        //this function realize user's input of numbers account
        public static int InputAcountOfNumbers()
        {
            int acountOfNumbers;
            Console.WriteLine("Input acount of numbers:");
            while (!int.TryParse(Console.ReadLine(), out acountOfNumbers))
            {
                Console.WriteLine("Value is wrong. Please, try again:");
            }
            return acountOfNumbers;
        }

        //return random file name
        public static string RandomString(int size)
        {
            var builder = new StringBuilder();
            var random = new Random();
            char ch;
            for (var i = 0; i < size; i++)
            {
                //Generate number, that is symbol in Unicode
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                //add this symbol to stringBuilder variable
                builder.Append(ch);
            }
            return builder.ToString();
        }

        //open file or create if not exists
        public static FileInfo CreateOrOpenFile(string path)
        {
            var file = new FileInfo(path + RandomString(acountOfSymbolsInFileName) + ".txt");
            return file;
        }

        //fill file with numbers
        public static void FileFill(FileInfo file, int acountOfNumbers)
        {
            var fileWriter = new StreamWriter(file.FullName);
            var random = new Random();
            for (var i = 0; i < acountOfNumbers; i++)
            {
                fileWriter.WriteLine(random.Next(-10, 10));
            }
            fileWriter.Close();
        }

        //read numbers in file to array
        public static int[] FileRead(FileInfo file)
        {
            var streamReader = new StreamReader(file.FullName);
            var numbersList = new ArrayList();
            while (!streamReader.EndOfStream)
            {
                int i;
                if (int.TryParse(streamReader.ReadLine(), out i))
                {
                    numbersList.Add(i);
                }
            }
            streamReader.Close();
            return numbersList.ToArray(typeof(int)) as int[];
        }

        //change file - write square of numbers 
        public static void FileChange(FileInfo file, int[] numbersArray)
        {
            StreamWriter streamWriter = new StreamWriter(file.FullName);
            foreach (var t in numbersArray)
            {
                if (t * t <= int.MaxValue)
                {
                    streamWriter.WriteLine(t * t);
                }
                else
                {
                    streamWriter.WriteLine("NaN");
                }
            }
            streamWriter.Close();
        }

        //output array to console
        public static void OutputArray(int[] numbersArray)
        {
            foreach (var t in numbersArray)
            {
                Console.Write("{0} ", t);
            }
            Console.WriteLine();
        }
    }

}
