﻿using System;


namespace Task4._4
{
    internal class CarSheet: IComparable<CarSheet>
    {
        public string CarModel { get; set; }
        public int CarNumber { get; set; }
        public string OwnerLastName { get; set; }
        public DateTime PurchaseYear { get; set; }
        public int MileAge { get; set; }

        public int CompareTo(CarSheet other)
        {
            return MileAge.CompareTo(other.MileAge);
        }

        public CarSheet(string carModel, int carNumber, string ownerlastName, DateTime purchaseYear, int mileAge)
        {
            CarModel = carModel;
            CarNumber = carNumber;
            OwnerLastName = ownerlastName;
            PurchaseYear = purchaseYear;
            MileAge = mileAge;
        }
    }
}
