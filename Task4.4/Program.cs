﻿using System;
using System.Collections.Generic;


namespace Task4._4
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var cars = FillCarsSheets();
            var oldCars = cars.FindAll(c => c.PurchaseYear <=  new DateTime(2010,12,31));
            oldCars.Sort();
            foreach (var car in oldCars)
            {
                Console.WriteLine("Car: {0}, Purchase year: {1}, mile age: {2}", car.CarModel, car.PurchaseYear, car.MileAge);
            }
            Console.ReadKey();
        }

        public static List<CarSheet> FillCarsSheets()
        {
            return new List<CarSheet>
            {
                new CarSheet("Opel", 11111, "Ivanov", new DateTime(2002, 1, 1), 180000),
                new CarSheet("Mercedes", 12345, "Petrov", new DateTime(2016, 1, 1), 100),
                new CarSheet("Volvo", 777111, "Sidorov", new DateTime(2010, 1, 1), 80000),
                new CarSheet("BMW", 765432, "Shevchuk", new DateTime(2007, 1, 1), 25000),
                new CarSheet("Mitsubisi", 222222, "Kozak", new DateTime(2005, 1, 1), 300000)
            };
        }
    }
}
