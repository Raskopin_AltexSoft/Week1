﻿/*
 * exist text file has real numbers and someone another. Program must find summ of this numbers. 
 */
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Task3._1
{
    class Program
    {
        static void Main()
        {
            //file path
            var path = Directory.GetCurrentDirectory() + @"\..\..\folder\real numbers.txt";

            //check if file "real numbers.text" exists
            if (FileExist(path))
            {
                string fileContent = FileContent(path);
                var regex = new Regex(@"\s+[+-]?\d+[.]?\d*(?=\s|$)");
                Console.WriteLine("\nSum of numbers finded in text file: {0}", SummInString(regex, fileContent));
            }
            else
            {
                Console.WriteLine("file not exist");
            }

            Console.ReadKey();
        }

        // check if file exists in given path
        public static bool FileExist(string path)
        {
            if (File.Exists(path))
            {
                return true;
            }
            return false;
        }

        // calculate sum of float numbers in given string by given regex tempalte 
        public static float SummInString(Regex regex, string content)
        {
            float sum = 0;
            float realNumber;
            foreach (Match match in regex.Matches(content))
            {
                if (!float.TryParse(match.Value, out realNumber))
                {
                    realNumber = 0;
                }
                sum += realNumber;
                Console.Write(match.Value.Trim() + " ");
            }
            return sum;
        }

        // return content of file located in given path
        public static string FileContent(string path)
        {
            var targetFile = new StreamReader(path);
            return targetFile.ReadToEnd();
        }

    }

}
